package com.productManagement.services;

import com.productManagement.exceptions.InvalidProductCategoryException;
import com.productManagement.exceptions.ResourceNotFoundException;
import com.productManagement.models.AvailabilityStatus;
import com.productManagement.models.Product;
import com.productManagement.models.ProductRequest;
import com.productManagement.models.ProductResponse;
import com.productManagement.models.category.Category;
import com.productManagement.repositories.ProductRepository;
import com.productManagement.repositories.category.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProductServiceTest {
	
	private static final long PRODUCT_ID = 299023L;
	private static final int AVAILABLE_STATUS = 1;
	private ProductRequest productRequest;
	private Category category;
	private Product product;
	
	@Mock
	private ProductRepository productRepository;
	
	@Mock
	private CategoryRepository categoryRepository;
	
	private ProductService productService;
	
	@Before
	public void setUp() {
		initMocks(this);
		productService = new ProductService(productRepository, categoryRepository);
		
		productRequest = createProductRequest();
		
		category = new Category();
		category.setId(productRequest.getCategoryId());
		when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));
		
		product = new Product(productRequest, category);
		when(productRepository.save(any(Product.class))).thenReturn(product);
		when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
	}
	
	@Test
	public void shouldSaveProductOnDatabase() {
		productService.createProduct(productRequest);
		
		verify(productRepository).save(any(Product.class));
	}
	
	@Test
	public void shouldConvertRequestToEntityOnCreateProduct() {
		productService.createProduct(productRequest);
		
		ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
		verify(productRepository).save(productArgumentCaptor.capture());
		
		Product product = productArgumentCaptor.getValue();
		
		assertThat(product.getName(), is(productRequest.getName()));
		assertThat(product.getDescription(), is(productRequest.getDescription()));
		assertThat(product.getZamroId(), is(productRequest.getZamroId()));
		assertThat(product.getMinOrderQuantity(), is(productRequest.getMinOrderQuantity()));
		assertThat(product.getCategory(), is(category));
		assertThat(product.getUnitOfMeasure(), is(productRequest.getUnitOfMeasure()));
		assertThat(product.getStatus(), is(AvailabilityStatus.AVAILABLE));
	}
	
	@Test
	public void shouldReturnSavedProduct() {
		ResponseEntity<ProductResponse> response = productService.createProduct(productRequest);
		
		ProductResponse savedProduct = response.getBody();
		
		assertThat(savedProduct.getZamroId(), is(product.getZamroId()));
		assertThat(savedProduct.getName(), is(product.getName()));
		assertThat(savedProduct.getZamroId(), is(product.getZamroId()));
		assertThat(savedProduct.getAvailable(), is(product.getStatus().getId()));
		assertThat(savedProduct.getCategoryId(), is(product.getCategory().getId()));
		assertThat(savedProduct.getMinOrderQuantity(), is(product.getMinOrderQuantity()));
		assertThat(savedProduct.getPurchasePrice(), is(product.getPurchasePrice()));
		assertThat(savedProduct.getUnitOfMeasure(), is(product.getUnitOfMeasure()));
	}
	
	@Test
	public void shouldRetrieveCategoryReferenceForProductCreation() {
		productService.createProduct(productRequest);
		
		verify(categoryRepository).findById(productRequest.getCategoryId());
	}
	
	@Test(expected = InvalidProductCategoryException.class)
	public void shouldNotCreateProductWhenCategoryIdIsNotFound() {
		when(categoryRepository.findById(productRequest.getCategoryId())).thenReturn(Optional.empty());
		
		productService.createProduct(productRequest);
	}
	
	@Test
	public void shouldRetrieveSingleProductFromDatabase() {
		productService.retrieveById(PRODUCT_ID);
		
		verify(productRepository).findById(PRODUCT_ID);
	}
	
	@Test
	public void shouldReturnHttpStatusOkWhenProductIsFoundById() {
		ResponseEntity<ProductResponse> response = productService.retrieveById(PRODUCT_ID);
		
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
	}
	
	@Test
	public void shouldReturnProductInformation() {
		ResponseEntity<ProductResponse> response = productService.retrieveById(PRODUCT_ID);
		
		ProductResponse foundProduct = response.getBody();
		
		assertThat(foundProduct.getName(), is(product.getName()));
		assertThat(foundProduct.getDescription(), is(product.getDescription()));
		assertThat(foundProduct.getZamroId(), is(product.getZamroId()));
		assertThat(foundProduct.getAvailable(), is(1));
		assertThat(foundProduct.getPurchasePrice(), is(product.getPurchasePrice()));
		assertThat(foundProduct.getCategoryId(), is(product.getCategory().getId()));
		assertThat(foundProduct.getUnitOfMeasure(), is(product.getUnitOfMeasure()));
		assertThat(foundProduct.getMinOrderQuantity(), is(product.getMinOrderQuantity()));
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void shouldThrowResourceNotFoundExceptionWhenProductDoesNotExist() {
		when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.empty());
		
		productService.retrieveById(PRODUCT_ID);
	}
	
	@Test
	public void shouldRetrieveAllProductsFromDatabase() {
		productService.retrieveAll();
		
		verify(productRepository).findAll();
	}
	
	@Test
	public void shouldReturnHttpStatusOkWhenProductsAreFound() {
		ResponseEntity<List<ProductResponse>> response = productService.retrieveAll();
		
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
	}
	
	@Test
	public void shouldReturnEmptyListOfProductsWhenNoneIsFound() {
		when(productRepository.findAll()).thenReturn(Collections.emptyList());
		
		ResponseEntity<List<ProductResponse>> response = productService.retrieveAll();
		
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
	}
	
	@Test
	public void shouldReturnListOfFoundProducts() {
		when(productRepository.findAll()).thenReturn(Collections.singletonList(product));
		
		ResponseEntity<List<ProductResponse>> response = productService.retrieveAll();
		
		List<ProductResponse> products = response.getBody();
		ProductResponse foundProduct = products.get(0);
		
		assertThat(foundProduct.getName(), is(product.getName()));
		assertThat(foundProduct.getDescription(), is(product.getDescription()));
		assertThat(foundProduct.getZamroId(), is(product.getZamroId()));
		assertThat(foundProduct.getAvailable(), is(1));
		assertThat(foundProduct.getPurchasePrice(), is(product.getPurchasePrice()));
		assertThat(foundProduct.getCategoryId(), is(product.getCategory().getId()));
		assertThat(foundProduct.getUnitOfMeasure(), is(product.getUnitOfMeasure()));
		assertThat(foundProduct.getMinOrderQuantity(), is(product.getMinOrderQuantity()));
	}
	
	@Test
	public void shouldRetrieveProductToBeUpdated() {
		productService.update(productRequest, PRODUCT_ID);
		
		verify(productRepository).findById(PRODUCT_ID);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void shouldNotUpdateProductWhenItIsNotFound() {
		when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.empty());
		
		productService.update(productRequest, PRODUCT_ID);
	}
	
	@Test
	public void shouldUpdateProductWithRequestData() {
		Product mockProduct = mockProduct();
		
		productService.update(productRequest, PRODUCT_ID);
		
		verify(mockProduct).update(productRequest);
	}
	
	@Test
	public void shouldRetrieveCategoryIfCategoryIdHasChanged() {
		productRequest.setCategoryId(12345L);
		productService.update(productRequest, PRODUCT_ID);
		
		verify(categoryRepository).findById(productRequest.getCategoryId());
	}
	
	@Test
	public void shouldNotRetrieveCategoryIfCategoryIdHasNotChanged() {
		productService.update(productRequest, PRODUCT_ID);
		
		verify(categoryRepository, never()).findById(anyLong());
	}
	
	@Test(expected = InvalidProductCategoryException.class)
	public void shouldNotUpdateProductWhenCategoryDoesNotExist() {
		productRequest.setCategoryId(98347L);
		when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());
		
		productService.update(productRequest, PRODUCT_ID);
	}
	
	@Test
	public void shouldReturnHttpStatusOkWhenProductIsSuccessfulUpdated() {
		ResponseEntity<ProductResponse> response = productService.update(productRequest, PRODUCT_ID);
		
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
	}
	
	@Test
	public void shouldReturnUpdatedProductData() {
		when(productRepository.save(any(Product.class))).thenReturn(product);
		
		ResponseEntity<ProductResponse> response = productService.update(productRequest, PRODUCT_ID);
		
		ProductResponse updatedProduct = response.getBody();
		
		assertThat(updatedProduct.getName(), is(product.getName()));
		assertThat(updatedProduct.getDescription(), is(product.getDescription()));
		assertThat(updatedProduct.getMinOrderQuantity(), is(product.getMinOrderQuantity()));
		assertThat(updatedProduct.getCategoryId(), is(product.getCategory().getId()));
		assertThat(updatedProduct.getUnitOfMeasure(), is(product.getUnitOfMeasure()));
		assertThat(updatedProduct.getZamroId(), is(product.getZamroId()));
		assertThat(updatedProduct.getPurchasePrice(), is(product.getPurchasePrice()));
		assertThat(updatedProduct.getAvailable(), is(AvailabilityStatus.AVAILABLE.getId()));
	}
	
	@Test
	public void shouldReturnProductForDeletion() {
		productService.delete(PRODUCT_ID);
		
		verify(productRepository).findById(PRODUCT_ID);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void shouldThrowResourceNotFoundExceptionWhenProductToBeDeletedDoesNotExist() {
		when(productRepository.findById(anyLong())).thenReturn(Optional.empty());
		
		productService.delete(9L);
	}
	
	@Test
	public void shouldDeleteProduct() {
		productService.delete(PRODUCT_ID);
		
		verify(productRepository).delete(product);
	}
	
	@Test
	public void shouldReturnHttpStatusNoContentWhenProductIsSuccessfullyDeleted() {
		ResponseEntity response = productService.delete(PRODUCT_ID);
		
		assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
	}
	
	private ProductRequest createProductRequest() {
		ProductRequest request = new ProductRequest();
		
		request.setName("product name");
		request.setDescription("product description");
		request.setZamroId("Zamro ID");
		request.setAvailable(AVAILABLE_STATUS);
		request.setCategoryId(123L);
		request.setMinOrderQuantity(100);
		request.setPurchasePrice(new BigDecimal("29.90"));
		request.setUnitOfMeasure("PCE");
		
		return request;
	}
	
	private Product mockProduct() {
		Product mockProduct = mock(Product.class);
		category.setId(productRequest.getCategoryId());
		when(mockProduct.getCategory()).thenReturn(category);
		when(mockProduct.getStatus()).thenReturn(AvailabilityStatus.AVAILABLE);
		when(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.of(mockProduct));
		return mockProduct;
	}
}