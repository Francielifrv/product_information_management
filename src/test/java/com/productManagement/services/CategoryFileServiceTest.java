package com.productManagement.services;

import com.productManagement.files.FileReader;
import com.productManagement.models.category.Category;
import com.productManagement.models.category.CategoryResponse;
import com.productManagement.repositories.category.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CategoryFileServiceTest {
	
	@Mock
	private FileReader fileReader;
	
	@Mock
	private CategoryRepository categoryRepository;
	
	private MockMultipartFile file;
	private Map<Long, String> categoriesMap;
	
	private CategoryFileService service;
	
	@Before
	public void setUp() throws IOException {
		initMocks(this);
		
		categoriesMap = categoryMap();
		when(fileReader.readToMap(any())).thenReturn(categoriesMap);
		
		service = new CategoryFileService(fileReader, categoryRepository);
	}
	
	@Test
	public void shouldReadFile() throws IOException {
		service.createFrom(file);
		
		verify(fileReader).readToMap(file);
	}
	
	@Test
	public void shouldSaveCategoriesListOnDatabase() throws IOException {
		service.createFrom(file);
		
		verify(categoryRepository).saveAll(anyIterable());
	}
	
	@Test
	public void shouldConvertMapOfCategoriesToCategoryEntity() throws IOException {
		service.createFrom(file);
		
		ArgumentCaptor<List<Category>> categoryCaptor = ArgumentCaptor.forClass(List.class);
		
		verify(categoryRepository).saveAll(categoryCaptor.capture());
		
		List<Category> categories = categoryCaptor.getValue();
		
		assertThat(categories, hasSize(3));
		assertThat(categories.get(0).getName(), is(categoriesMap.get(1L)));
		assertThat(categories.get(1).getName(), is(categoriesMap.get(2L)));
		assertThat(categories.get(2).getName(), is(categoriesMap.get(3L)));
	}
	
	@Test
	public void shouldReturnHttpStatusCreatedWhenCategoriesAreCreatedFromFile() throws IOException {
		Category category = new Category(1L, "Category One");
		when(categoryRepository.saveAll(anyList())).thenReturn(Collections.singletonList(category));
		
		ResponseEntity<List<CategoryResponse>> response = service.createFrom(file);
		
		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
	}
	
	@Test
	public void shouldReturnListOfCreatedCategories() throws IOException {
		Category category = new Category(1L, "Category One");
		when(categoryRepository.saveAll(anyList())).thenReturn(Collections.singletonList(category));
		
		ResponseEntity<List<CategoryResponse>> response = service.createFrom(file);
		
		List<CategoryResponse> categories = response.getBody();
		assertThat(categories, hasSize(1));
		assertThat(categories.get(0).getId(), is(category.getId()));
		assertThat(categories.get(0).getName(), is(category.getName()));
	}
	
	private Map<Long, String> categoryMap() {
		return new HashMap<Long, String>() {{
			put(1L, "Category One");
			put(2L, "Category Two");
			put(3L, "Category Three");
		}};
	}
}