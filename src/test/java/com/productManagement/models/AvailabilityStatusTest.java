package com.productManagement.models;

import com.productManagement.exceptions.InvalidProductStatusException;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class AvailabilityStatusTest {
	
	@Test
	public void shouldGetStatusById() {
		assertThat(AvailabilityStatus.getById(0), is(AvailabilityStatus.UNAVAILABLE));
		assertThat(AvailabilityStatus.getById(1), is(AvailabilityStatus.AVAILABLE));
	}
	
	@Test(expected = InvalidProductStatusException.class)
	public void shouldThrowInvalidProductStatusExceptionWhenStatusIsNotFound() {
		AvailabilityStatus.getById(98);
	}
}