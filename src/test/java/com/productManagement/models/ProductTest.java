package com.productManagement.models;

import com.productManagement.exceptions.InvalidProductStatusException;
import com.productManagement.models.category.Category;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ProductTest {
	
	Category category = new Category();
	
	@Before
	public void setUp() {
		category = new Category();
		category.setId(9348L);
	}
	
	@Test
	public void shouldBeCreatedFromRequestObject() {
		ProductRequest request = createProductRequest();
		
		Product product = new Product(request, category);
		
		assertThat(product.getName(), is(request.getName()));
		assertThat(product.getDescription(), is(request.getDescription()));
		assertThat(product.getZamroId(), is(request.getZamroId()));
		assertThat(product.getStatus(), is(AvailabilityStatus.AVAILABLE));
		assertThat(product.getUnitOfMeasure(), is(request.getUnitOfMeasure()));
		assertThat(product.getPurchasePrice(), is(request.getPurchasePrice()));
		assertThat(product.getMinOrderQuantity(), is(request.getMinOrderQuantity()));
		assertThat(product.getCategory(), is(category));
	}
	
	@Test(expected = InvalidProductStatusException.class)
	public void shouldThrowInvalidProductStatusExceptionWhenRequestContainsInvalidAvailabilityId() {
		ProductRequest request = new ProductRequest();
		
		request.setAvailable(344);
		
		new Product(request, category);
	}
	
	private ProductRequest createProductRequest() {
		ProductRequest request = new ProductRequest();
		
		request.setName("product name");
		request.setDescription("product description");
		request.setZamroId("Zamro ID");
		request.setAvailable(1);
		request.setCategoryId(123L);
		request.setMinOrderQuantity(100);
		request.setPurchasePrice(new BigDecimal("29.90"));
		request.setUnitOfMeasure("PCE");
		
		return request;
	}
}