package com.productManagement.models;

import com.productManagement.models.category.Category;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ProductResponseTest {
	
	@Test
	public void shouldBeBuiltFromEntity() {
		Product entity = createEntity();
		
		ProductResponse response = new ProductResponse(entity);
		
		assertThat(response.getId(), is(entity.getId()));
		assertThat(response.getZamroId(), is(entity.getZamroId()));
		assertThat(response.getCategoryId(), is(entity.getCategory().getId()));
		assertThat(response.getName(), is(entity.getName()));
		assertThat(response.getDescription(), is(entity.getDescription()));
		assertThat(response.getAvailable(), is(entity.getStatus().getId()));
		assertThat(response.getMinOrderQuantity(), is(entity.getMinOrderQuantity()));
		assertThat(response.getUnitOfMeasure(), is(entity.getUnitOfMeasure()));
		assertThat(response.getPurchasePrice(), is(entity.getPurchasePrice()));
	}
	
	private Product createEntity() {
		Category category = new Category();
		category.setId(8347L);
		
		Product product = new Product();
		
		product.setId(199L);
		product.setZamroId("B8947DA");
		product.setName("product name");
		product.setDescription("description");
		product.setStatus(AvailabilityStatus.UNAVAILABLE);
		product.setMinOrderQuantity(10);
		product.setUnitOfMeasure("m");
		product.setPurchasePrice(new BigDecimal(800.75D));
		product.setCategory(category);
		
		return product;
	}
}