package com.productManagement.controllers;

import com.productManagement.services.CategoryFileService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class CategoryFileUploadControllerTest {
	
	@Mock
	private CategoryFileService service;
	
	MultipartFile file;
	
	private CategoryFileUploadController controller;
	
	@Before
	public void setUp() {
		initMocks(this);
		
		file = new MockMultipartFile("name.csv", "Key,Value".getBytes());
		controller = new CategoryFileUploadController(service);
	}
	
	@Test
	public void shouldCreateCategoriesFromFile() throws IOException {
		controller.uploadFile(file);
		
		verify(service).createFrom(file);
	}
}