package com.productManagement.controllers;

import com.productManagement.models.ProductRequest;
import com.productManagement.models.ProductResponse;
import com.productManagement.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProductControllerTest {
	
	private static final long PRODUCT_ID = 897L;
	
	ProductRequest request;
	ResponseEntity<ProductResponse> response;
	
	@Mock
	ProductService service;
	
	ProductController controller;
	
	@Before
	public void setUp() {
		initMocks(this);
		
		controller = new ProductController(service);
		
		request = new ProductRequest();
		response = new ResponseEntity<>(new ProductResponse(), HttpStatus.ACCEPTED);
	}
	
	@Test
	public void shouldCreateProduct() {
		controller.createProduct(request);
		
		verify(service).createProduct(request);
	}
	
	@Test
	public void shouldReturnCreatedProduct() {
		when(service.createProduct(request)).thenReturn(response);
		
		ResponseEntity<ProductResponse> actualResponse = controller.createProduct(request);
		
		assertThat(actualResponse, is(response));
	}
	
	@Test
	public void shouldGetProductById() {
		controller.retrieveById(PRODUCT_ID);
		
		verify(service).retrieveById(PRODUCT_ID);
	}
	
	@Test
	public void shouldReturnFoundProductInformation() {
		when(service.retrieveById(PRODUCT_ID)).thenReturn(response);
		
		ResponseEntity<ProductResponse> actualResponse = controller.retrieveById(PRODUCT_ID);
		
		assertThat(actualResponse, is(response));
	}
	
	@Test
	public void shouldRetrieveAllProducts() {
		controller.retrieveAll();
		
		verify(service).retrieveAll();
	}
	
	@Test
	public void shouldReturnListOfProducts() {
		ResponseEntity<List<ProductResponse>> products = new ResponseEntity<>(Collections.singletonList(new ProductResponse()),
			HttpStatus.OK);
		
		when(service.retrieveAll()).thenReturn(products);
		
		ResponseEntity<List<ProductResponse>> response = controller.retrieveAll();
		
		assertThat(response, is(products));
	}
	
	@Test
	public void shouldUpdateProduct() {
		controller.update(request, PRODUCT_ID);
		
		verify(service).update(request, PRODUCT_ID);
	}
	
	@Test
	public void shouldReturnUpdatedProduct() {
		when(service.update(request, PRODUCT_ID)).thenReturn(response);
		
		ResponseEntity<ProductResponse> actualResponse = controller.update(request, PRODUCT_ID);
		
		assertThat(actualResponse, is(response));
	}
	
	@Test
	public void shouldDeleteProduct() {
		controller.delete(PRODUCT_ID);
		
		verify(service).delete(PRODUCT_ID);
	}
}