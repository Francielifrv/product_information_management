package com.productManagement.files;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class FileReaderTest {
	
	@Mock
	private FileReaderWrapper fileReaderWrapper;
	
	private MultipartFile file;
	
	private FileReader fileReader;
	
	@Before
	public void setUp() throws IOException {
		initMocks(this);
		
		file = buildMultiPartFile();
		
		fileReader = new FileReader(fileReaderWrapper);
		
		List<String> fileLines = new ArrayList<String>() {{
			add("header1,header2");
			add("72837,value");
		}};
		
		when(fileReaderWrapper.contentToStringList(any(MultipartFile.class))).thenReturn(fileLines);
	}
	
	@Test
	public void shouldConvertFileContentToString() throws IOException {
		fileReader.readToMap(file);
		
		verify(fileReaderWrapper).contentToStringList(file);
	}
	
	@Test
	public void shouldParseContentToMapOfLongAndStringWithoutHeader() throws IOException {
		Map<Long, String> linesAsMap = fileReader.readToMap(file);
		
		assertThat(linesAsMap.size(), is(1));
		assertThat(linesAsMap.get(72837L), is("value"));
	}
	
	@Test
	public void shouldRemoveFileHeader() throws IOException {
		Map<Long, String> linesAsMap = fileReader.readToMap(file);
	}
	
	private MultipartFile buildMultiPartFile() {
		String fileContent = "header1,header2\n" +
			"72837,value";
		
		return new MockMultipartFile("fileName.csv", fileContent.getBytes());
	}
}