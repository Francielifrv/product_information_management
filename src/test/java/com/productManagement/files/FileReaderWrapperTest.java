package com.productManagement.files;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FileReaderWrapperTest {
	
	MultipartFile file;
	
	private FileReaderWrapper fileReader;
	
	@Before
	public void setUp() {
		fileReader = new FileReaderWrapper();
		
		file = buildMultiPartFile();
	}
	
	@Test
	public void shouldConvertFileContentToList() throws IOException {
		List<String> lines = fileReader.contentToStringList(file);
		
		assertThat(lines, hasSize(2));
		assertThat(lines.get(0), is("a,csv,file"));
		assertThat(lines.get(1), is("with,multiple, lines"));
	}
	
	private MultipartFile buildMultiPartFile() {
		String fileContent = "a,csv,file\n" +
			"with,multiple, lines";
		
		return new MockMultipartFile("fileName.csv", fileContent.getBytes());
	}
}