package com.productManagement.services;

import com.productManagement.exceptions.InvalidProductCategoryException;
import com.productManagement.exceptions.ResourceNotFoundException;
import com.productManagement.models.category.Category;
import com.productManagement.models.ProductRequest;
import com.productManagement.models.ProductResponse;
import com.productManagement.models.Product;
import com.productManagement.repositories.ProductRepository;
import com.productManagement.repositories.category.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class ProductService {
	
	private final ProductRepository repository;
	private final CategoryRepository categoryRepository;
	
	
	public ProductService(ProductRepository repository, CategoryRepository categoryRepository) {
		this.repository = repository;
		this.categoryRepository = categoryRepository;
	}
	
	public ResponseEntity<ProductResponse> createProduct(ProductRequest request) {
		Category category = retrieveCategory(request);
		
		Product product = new Product(request, category);
		
		Product savedProduct = repository.save(product);
		
		ProductResponse response = new ProductResponse(savedProduct);
		
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	private Category retrieveCategory(ProductRequest request) {
		return categoryRepository.findById(request.getCategoryId())
			.orElseThrow(() -> new InvalidProductCategoryException("Cannot create product - Product category not found"));
	}
	
	public ResponseEntity<ProductResponse> retrieveById(Long productId) {
		Product product = retrieveProductBy(productId);
		
		ProductResponse response = new ProductResponse(product);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	private Product retrieveProductBy(Long productId) {
		return repository.findById(productId)
			.orElseThrow(() -> new ResourceNotFoundException("Product not found"));
	}
	
	public ResponseEntity<List<ProductResponse>> retrieveAll() {
		Iterable<Product> products = repository.findAll();
		
		List<ProductResponse> foundProducts =  StreamSupport.stream(products.spliterator(), false)
			.map(ProductResponse::new).collect(Collectors.toList());
		
		return new ResponseEntity<>(foundProducts, HttpStatus.OK);
	}
	
	public ResponseEntity<ProductResponse> update(ProductRequest request, Long productId) {
		Product product = retrieveProductBy(productId);
		
		updateProduct(request, product);
		
		ProductResponse productResponse = new ProductResponse(product);
		return new ResponseEntity<>(productResponse, HttpStatus.OK);
	}
	
	private void updateProduct(ProductRequest request, Product product) {
		if (!product.getCategory().getId().equals(request.getCategoryId())) {
			Category category = retrieveCategory(request);
			product.update(request, category);
		}
		
		product.update(request);
	}
	
	public ResponseEntity delete(Long productId) {
		Product product = retrieveProductBy(productId);
		
		repository.delete(product);
		
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
}
