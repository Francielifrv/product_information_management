package com.productManagement.services;

import com.productManagement.files.FileReader;
import com.productManagement.models.category.Category;
import com.productManagement.models.category.CategoryResponse;
import com.productManagement.repositories.category.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CategoryFileService {
	
	private final FileReader fileReader;
	private final CategoryRepository categoryRepository;
	
	public CategoryFileService(FileReader fileReader, CategoryRepository categoryRepository) {
		this.fileReader = fileReader;
		this.categoryRepository = categoryRepository;
	}
	
	public ResponseEntity<List<CategoryResponse>> createFrom(MultipartFile file) throws IOException {
		List<Category> categories = MapToCategories(file);
		
		Iterable<Category> createdCategories = categoryRepository.saveAll(categories);
		
		List<CategoryResponse> response = iterableToList(createdCategories);
		
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	private List<Category> MapToCategories(MultipartFile file) throws IOException {
		return fileReader.readToMap(file)
			.entrySet()
			.stream()
			.map(entry -> new Category(entry.getKey(), entry.getValue()))
			.collect(Collectors.toList());
	}
	
	private List<CategoryResponse> iterableToList(Iterable<Category> categories) {
		return StreamSupport.stream(categories.spliterator(), false)
			.map(CategoryResponse::new).collect(Collectors.toList());
	}
}
