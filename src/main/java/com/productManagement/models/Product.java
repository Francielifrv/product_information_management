package com.productManagement.models;

import com.productManagement.models.category.Category;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product {
	
	@Id
	@GeneratedValue
	private Long id;
	private String zamroId;
	private String name;
	private String description;
	private String unitOfMeasure;
	private Integer minOrderQuantity;
	private BigDecimal purchasePrice;
	private AvailabilityStatus status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Category category;
	
	Product() {
	}
	
	public Product(ProductRequest request, Category category) {
		this.status = AvailabilityStatus.getById(request.getAvailable());
		this.name = request.getName();
		this.description = request.getDescription();
		this.zamroId = request.getZamroId();
		this.unitOfMeasure = request.getUnitOfMeasure();
		this.minOrderQuantity = request.getMinOrderQuantity();
		this.purchasePrice = request.getPurchasePrice();
		this.category = category;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getZamroId() {
		return zamroId;
	}
	
	public void setZamroId(String zamroId) {
		this.zamroId = zamroId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getMinOrderQuantity() {
		return minOrderQuantity;
	}
	
	public void setMinOrderQuantity(Integer minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}
	
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	public AvailabilityStatus getStatus() {
		return status;
	}
	
	public void setStatus(AvailabilityStatus status) {
		this.status = status;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	public void update(ProductRequest request) {
		this.status = AvailabilityStatus.getById(request.getAvailable());
		this.name = request.getName();
		this.description = request.getDescription();
		this.zamroId = request.getZamroId();
		this.unitOfMeasure = request.getUnitOfMeasure();
		this.minOrderQuantity = request.getMinOrderQuantity();
		this.purchasePrice = request.getPurchasePrice();
	}
	
	public void update(ProductRequest request, Category category) {
		update(request);
		this.category = category;
	}
}
