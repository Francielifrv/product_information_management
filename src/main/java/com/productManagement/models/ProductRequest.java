package com.productManagement.models;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class ProductRequest {
	
	private String zamroId;
	private String name;
	private String description;
	
	@Min(value = 1, message = "Mininum quantity per order must be higher than 0")
	private int minOrderQuantity;
	private String unitOfMeasure;
	private Long categoryId;
	
	@Min(value = 0L, message = "Purchase price must be positive")
	private BigDecimal purchasePrice;
	private int available;
	
	public String getZamroId() {
		return zamroId;
	}
	
	public void setZamroId(String zamroId) {
		this.zamroId = zamroId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getMinOrderQuantity() {
		return minOrderQuantity;
	}
	
	public void setMinOrderQuantity(int minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}
	
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	public Long getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	public int getAvailable() {
		return available;
	}
	
	public void setAvailable(int available) {
		this.available = available;
	}
}
