package com.productManagement.models;

import java.math.BigDecimal;

public class ProductResponse {
	private Long id;
	private String zamroId;
	private String name;
	private String description;
	private int minOrderQuantity;
	private String unitOfMeasure;
	private Long categoryId;
	private BigDecimal purchasePrice;
	private int available;
	
	public ProductResponse() {
	}
	
	public ProductResponse(Product product) {
		this.id = product.getId();
		this.zamroId = product.getZamroId();
		this.name = product.getName();
		this.description = product.getDescription();
		this.minOrderQuantity = product.getMinOrderQuantity();
		this.unitOfMeasure = product.getUnitOfMeasure();
		this.purchasePrice = product.getPurchasePrice();
		this.available = product.getStatus().getId();
		this.categoryId = product.getCategory().getId();
	}
	
	public Long getId() {
		return id;
	}
	
	public String getZamroId() {
		return zamroId;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getMinOrderQuantity() {
		return minOrderQuantity;
	}
	
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	
	public Long getCategoryId() {
		return categoryId;
	}
	
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	
	public int getAvailable() {
		return available;
	}
}
