package com.productManagement.models;

import com.productManagement.exceptions.InvalidProductStatusException;

import java.util.EnumSet;
import java.util.function.Supplier;

public enum  AvailabilityStatus {
	UNAVAILABLE(0), AVAILABLE(1);
	
	private int id;
	
	AvailabilityStatus(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public static AvailabilityStatus getById(int id) {
		return EnumSet.allOf(AvailabilityStatus.class)
			.stream()
			.filter(status -> status.id == id)
			.findFirst()
			.orElseThrow(invalidProductStatusExceptionWrapper(id));
	}
	
	private static Supplier<InvalidProductStatusException> invalidProductStatusExceptionWrapper(int id) {
		return () -> new InvalidProductStatusException("Invalid product status id -> status id:" + id);
	}
}
