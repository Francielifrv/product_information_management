package com.productManagement.models.category;

public class CategoryResponse {
	private Long id;
	private String name;
	
	public CategoryResponse() {
	}
	
	public CategoryResponse(Category category) {
		this.id = category.getId();
		this.name = category.getName();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
