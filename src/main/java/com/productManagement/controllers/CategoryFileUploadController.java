package com.productManagement.controllers;


import com.productManagement.models.category.CategoryResponse;
import com.productManagement.services.CategoryFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/categories/upload")
public class CategoryFileUploadController {
	
	private final CategoryFileService fileService;
	
	@Autowired
	public CategoryFileUploadController(CategoryFileService fileService) {
		this.fileService = fileService;
	}
	
	@PostMapping
	public ResponseEntity<List<CategoryResponse>> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
		return fileService.createFrom(file);
	}
}
