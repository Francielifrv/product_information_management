package com.productManagement.controllers;

import com.productManagement.models.ProductRequest;
import com.productManagement.models.ProductResponse;
import com.productManagement.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private final ProductService service;
	
	public ProductController(ProductService service) {
		this.service = service;
	}
	
	@PostMapping
	public ResponseEntity<ProductResponse> createProduct(@Valid @RequestBody ProductRequest request) {
		return service.createProduct(request);
	}
	
	@GetMapping("/{productId}")
	public ResponseEntity<ProductResponse> retrieveById(@PathVariable("productId") Long productId) {
		return service.retrieveById(productId);
	}
	
	@GetMapping
	public ResponseEntity<List<ProductResponse>> retrieveAll() {
		return service.retrieveAll();
	}
	
	@PutMapping("/{productId}")
	public ResponseEntity<ProductResponse> update(@Valid @RequestBody ProductRequest request,
	                                              @PathVariable("productId") Long productId) {
		return service.update(request, productId);
	}
	
	@DeleteMapping("/{productId}")
	public ResponseEntity delete(@PathVariable("productId") Long productId) {
		return service.delete(productId);
	}
}
