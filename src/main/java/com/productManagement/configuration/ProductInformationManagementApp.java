package com.productManagement.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.productManagement")
@Configuration
@EnableJpaRepositories("com.productManagement.repositories")
@EntityScan("com.productManagement.models")
public class ProductInformationManagementApp {
	public static void main(String[] args) {
		SpringApplication.run(ProductInformationManagementApp.class, args);
	}
}
