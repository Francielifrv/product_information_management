package com.productManagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidProductStatusException extends RuntimeException {
	
	public InvalidProductStatusException(String message) {
		super(message);
	}
}
