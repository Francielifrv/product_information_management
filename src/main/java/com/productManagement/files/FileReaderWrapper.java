package com.productManagement.files;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Component
public class FileReaderWrapper {
	
	public List<String> contentToStringList(MultipartFile file) throws IOException {
		return IOUtils.readLines(file.getInputStream(), "UTF8");
	}
}
