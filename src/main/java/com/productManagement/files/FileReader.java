package com.productManagement.files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class FileReader {
	
	private final FileReaderWrapper fileReader;
	
	@Autowired
	public FileReader(FileReaderWrapper fileReader) {
		this.fileReader = fileReader;
	}
	
	public Map<Long, String > readToMap(MultipartFile file) throws IOException {
		List<String> lines = fileReader.contentToStringList(file);
		
		List<String> fileContent = removeHeader(lines);
		
		return listToMap(fileContent);
	}
	
	private Map<Long, String> listToMap(List<String> fileContent) {
		return fileContent.stream()
			.filter(line -> line.contains(","))
			.map(line -> line.split(","))
			.collect(Collectors.toMap(l -> Long.parseLong(l[0]), l -> l[1]));
	}
	
	private List<String> removeHeader(List<String> fileContent) {
		fileContent.remove(0);
		
		return fileContent;
	}
	
}
