import com.productManagement.configuration.ProductInformationManagementApp;
import com.productManagement.models.ProductRequest;
import com.productManagement.models.ProductResponse;
import com.productManagement.models.category.Category;
import com.productManagement.repositories.category.CategoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest(classes = {ProductInformationManagementApp.class},
	webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProductIntegrationTest {
	
	private static final String PRODUCT_ENDPOINT = "http://localhost:8080/products";
	
	private static final long CATEGORY_ID = 61947L;
	private static final String PRODUCT_NAME = "Rechte inschroefkoppeling GE35LR1.1/4 -M+D (zonder moer en snijring) RVS-316TI";
	private static final String DESCRIPTION = "RVS GELR -M+D<br />Rechte pijpinschroefkoppenlingen<br />Materiaal";
	private static final String ZAMRO_ID = "4EDA0";
	private static final int MIN_ORDER_QUANTITY = 1;
	private static final String UNIT_OF_MEASURE = "PCE";
	private static final BigDecimal PURCHASE_PRICE = new BigDecimal("79.22");
	private static final int AVAILABLE_STATUS = 1;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	TestRestTemplate restTemplate = new TestRestTemplate();
	
	@Test
	public void shouldInsertProductInformation() {
		ResponseEntity<ProductResponse> response = createProductPostRequest();
		
		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
		
		ProductResponse createdProduct = response.getBody();
		assertThat(createdProduct.getId(), greaterThan(0L));
		assertThat(createdProduct.getName(), is(PRODUCT_NAME));
		assertThat(createdProduct.getDescription(), is(DESCRIPTION));
		assertThat(createdProduct.getMinOrderQuantity(), is(MIN_ORDER_QUANTITY));
		assertThat(createdProduct.getZamroId(), is(ZAMRO_ID));
		assertThat(createdProduct.getUnitOfMeasure(), is(UNIT_OF_MEASURE));
		assertThat(createdProduct.getPurchasePrice(), is(PURCHASE_PRICE));
		assertThat(createdProduct.getAvailable(), is(AVAILABLE_STATUS));
	}
	
	@Test
	public void shouldRetrieveProductInformation() {
		Long productId = createProductPostRequest().getBody().getId();
		
		String retrieveProductEndpoint = PRODUCT_ENDPOINT + "/" + productId;
		
		ResponseEntity<ProductResponse> response = retrieveProductRequest(retrieveProductEndpoint);
		
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
		
		ProductResponse productInformation = response.getBody();
		assertThat(productInformation.getId(), is(productId));
		assertThat(productInformation.getName(), is(PRODUCT_NAME));
		assertThat(productInformation.getDescription(), is(DESCRIPTION));
		assertThat(productInformation.getMinOrderQuantity(), is(MIN_ORDER_QUANTITY));
		assertThat(productInformation.getZamroId(), is(ZAMRO_ID));
		assertThat(productInformation.getUnitOfMeasure(), is(UNIT_OF_MEASURE));
		assertThat(productInformation.getPurchasePrice(), is(PURCHASE_PRICE));
		assertThat(productInformation.getAvailable(), is(AVAILABLE_STATUS));
	}
	
	@Test
	public void shouldReturnHttpStatusNotFoundOnRetrieveNonExistentProductInformation() {
		String retrieveProductEndpoint = PRODUCT_ENDPOINT + "/3847";
		
		ResponseEntity<ProductResponse> response = retrieveProductRequest(retrieveProductEndpoint);
		
		assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
	}
	
	@Test
	public void shouldUpdateProduct() {
		Long productId = createProductPostRequest().getBody().getId();
		
		String productEndpoint = PRODUCT_ENDPOINT + "/" + productId;
		
		ProductRequest updateProductRequest = buildPostRequest();
		updateProductRequest.setName("updated name");
		
		ResponseEntity<ProductResponse> response = updateProductRequest(productEndpoint, updateProductRequest);
		
		assertThat(response.getBody().getName(), is("updated name"));
	}
	
	@Test
	public void shouldReturnHttpStatusNotFoundWhenDeletingInvalidProduct() {
		String productEndpoint = PRODUCT_ENDPOINT + "/8";
		
		ResponseEntity response = restTemplate.exchange(productEndpoint, HttpMethod.DELETE, null, Object.class);
		
		assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
	}
	
	private ResponseEntity<ProductResponse> updateProductRequest(String productEndpoint, ProductRequest updateProductRequest) {
		HttpEntity<ProductRequest> request = new HttpEntity<>(updateProductRequest);
		
		return restTemplate.exchange(productEndpoint, HttpMethod.PUT, request,
			ProductResponse.class);
	}
	
	private ResponseEntity<ProductResponse> retrieveProductRequest(String retrieveProductEndpoint) {
		return restTemplate.exchange(retrieveProductEndpoint, HttpMethod.GET, null, ProductResponse.class);
	}
	
	private ResponseEntity<ProductResponse> createProductPostRequest() {
		addCategoryToDatabase();
		
		HttpEntity<ProductRequest> request = new HttpEntity<>(buildPostRequest());
		
		return restTemplate.exchange(PRODUCT_ENDPOINT, HttpMethod.POST, request, ProductResponse.class);
	}
	
	private ProductRequest buildPostRequest() {
		ProductRequest request = new ProductRequest();
		
		request.setName(PRODUCT_NAME);
		request.setDescription(DESCRIPTION);
		request.setMinOrderQuantity(MIN_ORDER_QUANTITY);
		request.setZamroId(ZAMRO_ID);
		request.setUnitOfMeasure(UNIT_OF_MEASURE);
		request.setCategoryId(CATEGORY_ID);
		request.setPurchasePrice(PURCHASE_PRICE);
		request.setAvailable(AVAILABLE_STATUS);
		
		return request;
	}
	
	private void addCategoryToDatabase() {
		Category category = new Category(CATEGORY_ID, "some category");
		
		categoryRepository.save(category);
	}
}