## Product information management
#### Dependencies

    - Java 8
    - Postgres
    - Gradle

#### Running tests

##### Unit tests
gradle test or ./gradlew test

##### integration tests
gradle integrationTest or ./gradlew integrationTest

#### Building the application
gradle build or ./gradlew build
Running the application

gradle createDB or ./gradlew createDB

gradle bootRun or ./gradlew bootRun - starts the application
